var intro = {},
    id_content,
    id_project,
    value,
    value2,
    element,
    imageUse,
    id_category,
    cat_name,
    cat_img,
    about_types = {
        text: 'Text simplu',
        c_accent: 'Text culoare roșie',
        c_primary: 'Text culoare albastră',
        c_text: 'Text culoare neagră',
        img: 'Imagine'
    },
    s_s;
$(function() {
    //
    // $('#imgUpload').modal();
    // $('#imgDelete').modal();
    // $('#imgZoom').modal();
    // $("#modalSuccess").modal();
    // $("#modalError").modal();
    //

    $('.overlay-stuff .content').toggle();
    $('.overlay-stuff').mouseup(function(e) {
        var container = $(".overlay-stuff .content");
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            hideSlide('#' + $(this).attr('id'))
        }
    });
    $('#imgUpload').on('show.bs.modal', function() {
        $('#imgUpload .progresses').empty();
    })
    $('#imgUpload').on('hide.bs.modal', function() {
        imgsac();
    })
    $('.overlay-stuff').hide(0);
    $('[dn]').hide();
    $('[dn]').removeClass('dn')
    $('.about-text-panel').show();
    $('.about-img-panel').hide();
    $('#aboutItemTypeSelect').change(function() {
        if ($(this).val() == 'text') {
            $('.about-text-panel').show();
            $('.about-img-panel').hide();
        } else if ($(this).val() == 'img') {
            $('.about-img-panel').show();
            $('.about-text-panel').hide();
        }
    });
    $('#aboutItemTypeSelect').val('text');
    $('#about-delete .btn').click(function() {
        if ($(this).attr('q-action') == 'y') {
            $.post('action.php?about-delete', {
                id_content: id_content
            }, function(data) {
                if (data.result == 'msg_ok') {
                    angular.element('#page-about').scope().loadData();
                }
            }).fail(function() {})
        }
    })
    $('#category-delete .btn').click(function() {
        if ($(this).attr('q-action') == 'y') {
            $.post('action.php?category-delete', {
                id_category: id_category
            }, function(data) {
                if (data.result == 'msg_ok') {
                    angular.element('#page-categories').scope().loadData();
                }
            }).fail(function() {
                alert('fuck!')
            })
        }
    })
    $('#project-delete .btn').click(function() {
        if ($(this).attr('q-action') == 'y') {
            $.post('action.php?project-delete', {
                id_project: id_project
            }, function(data) {
                angular.element('#page-projects').scope().loadAllData();
            }).fail(function() {
                alert('fuck!')
            })
        }
    })
    $('.about-item-btn').click(function() {
        $('#about-form').attr('myAction', $(this).attr('myAction'));
    })
    $('#about-form-img-edit').submit(function(e) {
        if (!$(this).valid()) return false;
        e.preventDefault();
        loading();
        $.post('action.php?about-edit-img', {
            id_content: id_content,
            value2: $('#about-form-img-edit #about-img-edit-src').val()
        }, function(data) {
            if (data.result == 'msg_ok') {
                $('#aboutItem').modal('hide');
                angular.element('#page-about').scope().loadData();
                success('Informația a fost modificată!')
            }
        }).fail(function() {
            alert("error");
        });
        loading();
        $('#aboutEditImg').modal('hide')
    })
    $('#about-form-text').submit(function(e) {
        if (!$(this).valid()) return false;
        e.preventDefault();
        loading();
        $.post('action.php?about-add', {
            element: $('#about-form-text #aboutContentTypeText').val(),
            value: $('#about-form-text #aboutContentTitle').val(),
            value2: $('#about-form-text #aboutContentContent').val()
        }, function(data) {
            if (data.result == 'msg_ok') {
                $('#aboutItem').modal('hide');
                angular.element('#page-about').scope().loadData();
                success('Informația a fost adăugată!')
            }
        }).fail(function() {
            alert("error");
        });
        loading();
    })
    $('#about-form-text-edit').submit(function(e) {
        if (!$(this).valid()) return false;
        e.preventDefault();
        loading();
        $.post('action.php?about-edit-text', {
            element: $('#about-form-text-edit #aboutContentTypeText').val(),
            value: $('#about-form-text-edit #aboutContentTitle').val(),
            value2: $('#about-form-text-edit #aboutContentContent').val(),
            id_content: id_content
        }, function(data) {
            if (data.result == 'msg_ok') {
                $('#aboutItem').modal('hide');
                angular.element('#page-about').scope().loadData();
                $('#aboutEditText').modal('hide');
                success('Informația a fost modificată!')
            }
        }).fail(function() {
            alert("error");
        });
        loading();
    })
    $('#about-form-img').submit(function(e) {
        if (!$(this).valid()) return false;
        e.preventDefault();
        loading();
        $.post('action.php?about-add-img', {
            value2: $('#about-form-img #about-img-src').val()
        }, function(data) {
            if (data.result == 'msg_ok') {
                $('#aboutItem').modal('hide');
                angular.element('#page-about').scope().loadData();
                success('Informația a fost adăugată!')
            }
        }).fail(function() {
            alert("error");
        });
        loading();
    })
    $('.select-img').click(function(e) {
        imageUse = '#' + $(this).parent().find('[type=hidden]').attr('id');
        if ($(this).hasClass('select-img-user')) {
            $('#selectImage [what=data]').hide();
            $('#selectImage [what=data2]').show();
        } else {
            $('#selectImage [what=data]').show();
            $('#selectImage [what=data2]').hide();
        }
        $('#selectImage').modal();
    })
    $('#index-intro').submit(function(e) {
        e.preventDefault();
        loading();
        Object.keys(intro).forEach(function(key) {
            el = '#index-intro #' + intro[key].element;
            if ($(el).attr('type') != 'text' && $(el).attr('type') != 'hidden') return;
            $.post('action.php?index-intro', {
                value: $(el).val(),
                id: intro[key].id
            });
        });
        loading();
        success('Datele au fost modificate!');
    })
    $('#index-block-visibility').submit(function(e) {
        e.preventDefault();
        loading();
        Object.keys(intro).forEach(function(key) {
            el = '#index-block-visibility #' + intro[key].element;
            if ($(el).attr('type') != 'checkbox') return;
            $.post('action.php?index-block-visibility', {
                value: $(el).is(':checked') ? '1' : '0',
                id: intro[key].id
            });
        });
        loading();
        success('Datele au fost modificate!');
    })
    $('#profile-edit').submit(function(e) {
        e.preventDefault();
        loading();
        $.post('action.php?profile-update', {
            fname: $('#profile-edit #fname').val(),
            lname: $('#profile-edit #lname').val(),
            username: $('#profile-edit #username').val(),
            email: $('#profile-edit #email').val(),
            img: $('#profile-edit #img-user').val()
        })
        loading();
        success('Datele au fost modificate!');
    })
    $('.sidebar-toggle').click(function() {
        $('.core').toggleClass('sidebar-collapsed');
    })
    $('#imgDelete [q-action=y]').click(function() {
        $('.files').find('[ng-src="' + $(this).attr('img-path') + '"]').parent().parent().hide(500)
        $.post('action.php?deleteFile', {
            path: $(this).attr('img-path')
        }, function(data) {})
    });
    $('.overlay-stuff .close').click(function(e) {
        $(this).parent().animate({
            width: 'toggle'
        }, 200).parent().fadeOut(200)
        $('body').css('overflow', 'auto')
    });
    $('.category-item-btn').click(function() {
        $('#form-add-category').attr('action', 'add')
        $('#form-add-category [type=submit]').text("Adaugă");
        showSlide('#categoryActions');
    });
    $('.project-item-btn').click(function() {
        $('#form-add-project').attr('mode', 'add');
        showSlide('#projectActions');
    });
    $('#form-add-category').submit(function() {
        if (!$(this).valid()) return false;
        loading();
        if ($(this).attr('action') == 'add') {
            $.post('action.php?category-add', {
                name: $('#form-add-category #cat-name').val(),
                img: $('#form-add-category #cat-img').val()
            }, function(data) {
                if (data.result == 'msg_ok') {
                    angular.element('#page-categories').scope().loadData();
                    hideSlide('#categoryActions');
                    success('Categoria a fost adăugată!')
                } else {
                    error('A survenit o eroare!')
                }
            }).fail(function() {
                alert("error");
            });
        } else if ($(this).attr('action') == 'edit') {
            $.post('action.php?category-edit', {
                id_category: id_category,
                name: $('#form-add-category #cat-name').val(),
                img: $('#form-add-category #cat-img').val()
            }, function(data) {
                if (data.result == 'msg_ok') {
                    angular.element('#page-categories').scope().loadData();
                    hideSlide('#categoryActions');
                    success('Categoria a fost modificată!')
                } else {
                    error('A survenit o eroare!')
                }
            }).fail(function() {
                alert("error");
            });
        }
        loading();
    });
    /* begin upload imgs */
    var ul = $('.upload .progresses');
    $('.btn-upload-img').click(function() {
        if ($(this).attr('upload-where') == 'user') {
            $('.form-upload-img').attr('action', 'action.php?upload-img-user');
        } else if ($(this).attr('upload-where') == 'site') {
            $('.form-upload-img').attr('action', 'action.php?upload-img-site');
        }
    })
    $('.upload .drop a').click(function() {
        $(this).parent().find('input').click();
    });

    $('.upload').fileupload({
        dropZone: $('.upload .drop'),
        add: function(e, data) {
            var tpl = $('.invisible-stuff .img-progress-bar').clone();
            tpl.find('.file-name').text(data.files[0].name + ' (' + formatFileSize(data.files[0].size) + ')')
            data.context = tpl.appendTo(ul);
            var jqXHR = data.submit();
        },
        progress: function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            data.context.find('.progress-bar').text(progress + '%');
            data.context.find('.progress-bar').attr('aria-valuenow', progress);
            data.context.find('.progress-bar').width(progress + '%');
            if (progress == 100) {
                data.context.find('.progress-bar').removeClass('progress-bar-warning progress-bar-striped active');
                angular.element('#page-files').scope().loadData2();
                angular.element('#page-files').scope().loadData();
            }
        },
        fail: function(e, data) {
            data.context.addClass('error');
        }
    });
    /* begin projects */
    // showSlide('#projectActions');
    $('#form-add-project').submit(function(e) {
        if (!$(this).valid()) return false;
        e.preventDefault();
        loading();
        if ($(this).attr('mode') == 'add') {
            $.post('action.php?project-add', {
                name: $('#form-add-project #proj-name').val(),
                description: $('#form-add-project #proj-description').val(),
                id_category: $('#form-add-project #proj-category').val(),
                goal: $('#form-add-project #proj-goal').val(),
                img: $('#form-add-project #proj-img').val(),
                date_limit: $('#form-add-project #proj-date_limit').val(),
                date_added: today()
            }, function(data) {
                if (data.result == 'msg_ok') {
                    success('Proiectul a fost creat!');
                    angular.element('#page-projects').scope().loadAllData();
                } else {
                    error('A survenit o eroare!')
                }
            }).fail(function() {
                alert("error");
            });
        } else if ($(this).attr('mode') == 'edit') {
            $.post('action.php?project-edit', {
                name: $('#form-add-project #proj-name').val(),
                description: $('#form-add-project #proj-description').val(),
                id_category: $('#form-add-project #proj-category').val(),
                goal: $('#form-add-project #proj-goal').val(),
                img: $('#form-add-project #proj-img').val(),
                date_limit: $('#form-add-project #proj-date_limit').val(),
                id_project: id_project
            }, function(data) {
                if (data.result == 'msg_ok') {
                    success('Proiectul a fost modificat!');
                    angular.element('#page-projects').scope().loadAllData();
                    angular.element('#page-projects').scope().loadUserData();
                } else {
                    error('A survenit o eroare!')
                }
            }).fail(function() {
                alert("error");
            });
        }
        loading();
    });
    $('.date-picker').datepicker({
        format: "yyyy-mm-dd",
        startView: 1
    });
    /* end projects */
    $('.only-numbers').keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
        })
        // hideSlide('#projectActions');

});
$(document).on('drop dragover', function(e) {
    e.preventDefault();
});

function today() {
    z = new Date();
    return z.getFullYear() + '-' + (z.getMonth() + 1) + '-' + z.getDate();
}

function formatFileSize(bytes) {
    return typeof bytes !== 'number' ? '' : bytes >= 1000000000 ? (bytes / 1000000000).toFixed(2) + ' GB' : bytes >= 1000000 ? (bytes / 1000000).toFixed(2) + ' MB' : (bytes / 1000).toFixed(2) + ' KB';
}

function showSlide(what) {
    if (s_s) return false;
    s_s = true;
    $(what).fadeIn(200);
    $(what + ' .content').animate({
        width: 'toggle'
    }, 200)
    $('body').css('overflow', 'hidden')
    $('input, textarea', what).val('');
    $('img', what).attr('src', '');
}

function hideSlide(what) {
    s_s = false;
    $(what).fadeOut(200);
    $(what + ' .content').animate({
        width: 'toggle'
    }, 200)
    $('body').css('overflow', 'auto')
}

function fillProjectEditForm() {
    var datax;
    $.post('../data.php?project-single', {
        id_project: id_project
    }, function(data) {
        showSlide('#projectActions');
        $('#form-add-project #proj-name').val(data.name);
        $('#form-add-project #proj-description').val(data.description);
        $('#form-add-project #proj-category').val(data.id_category);
        $('#form-add-project #proj-goal').val(data.goal);
        $('#form-add-project #proj-img').val(data.img);
        $('#form-add-project #proj-img-actual').attr('src', data.img);
        $('#form-add-project #proj-date_limit').val(data.date_limit);
    }).fail(function() {
        alert("error");
    });
}

var app = angular.module('backer', []);
app.controller('files', function($scope, $http) {
    $scope.loadData = function() {
        $http.get('../data.php?files-images').then(function(response) {
            $scope.data = response.data.img;
        })
    }
    $scope.loadData2 = function() {
        $http.get('../data.php?files-images-users').then(function(response) {
            $scope.data2 = response.data.img;
        })
    }
    $scope.loadData2();
    $scope.loadData();
})
app.controller('about', function($scope, $http) {
    $scope.loadData = function() {
        $http.get('../data.php?about').then(function(response) {
            $scope.data = response.data.data;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.data[i].about_types = about_types[$scope.data[i].element];
            }
        })
    }
    $scope.loadData();
})
app.controller('pledges', function($scope, $http) {
    $http.get('../data.php?pledges-all').then(function(response) {
        $scope.data_all = response.data.data;
    })
    $http.get('../data.php?pledges-mine').then(function(response) {
        $scope.data_mine = response.data.data;
    })
})
app.controller('projects', function($scope, $http) {
    $scope.loadAllData = function() {
        $http.get('../data.php?projects').then(function(response) {
            $scope.data_all = response.data.data;
        })
    };
    $scope.loadUserData = function() {
        $http.get('../data.php?projects-mine').then(function(response) {
            $scope.data_user = response.data.data;
        })
    };
    $scope.loadAllData();
    $scope.loadUserData();
})
app.directive('projects', function() {
    return function(scope, element, attrs) {
        if (scope.$last) {
            $('.project-delete-item').click(function() {
                id_project = $(this).attr('id');
                $('#project-delete').modal();
            });
            $('.project-edit-item').click(function() {
                id_project = $(this).attr('id');
                $('#form-add-project').attr('mode', 'edit');
                fillProjectEditForm();
            });
        }
    }
})
app.controller('index', function($scope, $http) {
    $http.get('../data.php?index').then(function(response) {
        $scope.index = response.data.header;
        Object.keys($scope.index).forEach(function(key) {
            intro[key] = $scope.index[key];
        });
        angular.forEach($scope.index, function(value, key) {
            x = intro[key].value;
            intro[key].value = x == '0' ? false : x == '1' ? true : x;
        });
    })
})

app.controller('my-profile', function($scope, $http) {
    $http.get('../data.php?my-profile').then(function(response) {
        $scope.data = response.data.profile;
    })
})
app.controller('categories', function($scope, $http) {
    $scope.loadData = function() {
        $http.get('../data.php?categories').then(function(response) {
            $scope.data = response.data.data;
        })
    }
    $scope.loadData();
})
app.directive('imgactions', function() {
    return function(scope, element, attrs) {
        if (scope.$last) {
            imgsac();
        }
    };
})
app.directive('categories', function() {
    return function(scope, element, attrs) {
        if (scope.$last) {
            $('.category-delete-item').click(function() {
                id_category = $(this).attr('id');
                $('#category-delete').modal();
            });
            $('.category-edit-item').click(function() {
                $('#form-add-category').attr('action', 'edit')
                id_category = $(this).attr('id');
                cat_name = $(this).parent().parent().parent().find('[data=name]').text();
                cat_img = $(this).parent().parent().parent().find('td img').attr('src');
                showSlide('#categoryActions');
                $('#form-add-category #cat-name').val(cat_name);
                $('#form-add-category #cat-img').val(cat_img);
                $('#form-add-category .img-container img').attr('src', cat_img);
                $('#form-add-category [type=submit]').text("Modifică");
            })
        }
    }
})
app.directive('dabout', function() {
    return function(scope, element, attrs) {
        if (scope.$last) {
            $('.about-delete-item').click(function() {
                id_content = $(this).attr('id');
                $('#about-delete').modal();
            })
            $('.about-edit-item').click(function() {
                id_content = $(this).attr('id');
                if ($(this).attr('value') == 'img') {
                    src = $(this).attr('value2');
                    $('#aboutEditImg').modal();
                    $('#aboutEditImg .img-container img').attr('src', src);
                    $('#aboutEditImg .about-img-edit-src').val(src);
                } else {
                    value = $(this).parent().parent().parent().find('[q-data="value"]').html();
                    value2 = $(this).parent().parent().parent().find('[q-data="value2"] span').html();
                    element = $(this).parent().parent().parent().find('[q-data="element"]').attr('element-original');
                    $('#about-form-text-edit #aboutContentTitle').val(value)
                    $('#about-form-text-edit #aboutContentContent').val(value2)
                    $('#about-form-text-edit #aboutContentTypeText').val(element)
                    $('#aboutEditText').modal();
                }
            });
        }
    };
});

function imgsac() {
    $('.files .zoom').click(function() {
        imgZoom($(this).parent().parent().find('.img-container img').attr('ng-src'))
    })
    $('.files .delete').click(function() {
        imgDelete($(this).parent().parent().find('.img-container img').attr('ng-src'))
    })
    $('#selectImage .files .img-container').click(function() {
        src = $('img', this).attr('ng-src');
        $(imageUse).val(src);
        $(imageUse).parent().parent().find('.img-container img').attr('src', src)
        $('#selectImage').modal('hide');
    })
    $('[data-toggle="tooltip"]').tooltip();
}

function imgZoom(what) {
    $('#imgZoom').modal().find('img').attr('src', what)
}

function imgDelete(what) {
    $('#imgDelete').modal().find('[q-action=y]').attr('img-path', what);
}

function error(message) {
    $('#modalError')
        .modal()
        .on('shown.bs.modal', function() {
            $('[data-dismiss=modal]', this).focus();
        })
        .find('.modal-body').html(message);
}

function success(message) {
    $('#modalSuccess')
        .modal()
        .on('shown.bs.modal', function() {
            $('[data-dismiss=modal]', this).focus();
        })
        .find('.modal-body').html(message);
}

function loading() {
    $('#loading').toggle();
}
