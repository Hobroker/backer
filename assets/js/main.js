var min_w = 300,
    vid_w_orig, vid_h_orig;
$(function() {
    //
    $('#carousel_one').carousel('pause');
    // $('#addPledge').modal();
    //

    // begin video cover
    vid_w_orig = 640;
    vid_h_orig = 340;
    resizeToCover();
    $(window).resize(function() {
        resizeToCover();
    });
    $(window).trigger('resize');
    // end video cover
    $('.only-numbers').keypress(function(e) {
        if (e.which != 13 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
    })
    $('a[nowhere]').click(function(e) {
        e.preventDefault()
    })
    $('[dn]').hide();
    $('[dn]').removeClass('dn')
    CloseOpenModals();
    $('form .alert.alert-danger').hide();
    $('.alert-danger .close').click(function() {
        console.log($(this).parent().parent().attr('class'));
        $(this).parent().parent().slideUp('100');
    });
    $('.carousel-inner .item:first').addClass('active');
    $('.nav-tabs.select-first li:first a').click();
    $('[data-toggle="tooltip"]').tooltip();
    $('.sign [data-toggle="tooltip"]').each(function() {
        $(this).attr('data-original-title', $(this).parent().find('input').attr('placeholder'));
        $(this).attr('data-placement', 'right');
    });
    $('#signInForm').submit(function(e) {
        var el = ['username_password', 'username', 'password'],
            ok = true;
        e.preventDefault();
        if ($('#username', this).val().length < 1 || $('#password', this).val().length < 1) {
            $('.alert-danger[err=username_password]', this).slideDown('100');
            ok = false;
        } else {
            $('.alert-danger[err=username_password]', this).slideUp('100');
        }
        if (!ok) return;
        $('#signinModal .modal-loading').show();
        $.post("action.php?signin", {
            username: $('#username', this).val(),
            password: $('#password', this).val()
        }, function(data) {
            console.log(data)
            if (data.result == 'msg_ok') {
                goIn();
            } else {
                $('#signinModal .modal-loading').hide();
                $('#signInForm .alert-danger[err=username_password]').slideDown('100');
            }
        }).fail(function() {
            alert("error");
        });
    });
    $('#signUpForm').submit(function(e) {
        e.preventDefault();
        var ok = true;
        if ($('#username', this).val().length < 1) {
            $('.alert-danger[err=username]', this).slideDown('100');
            ok = false;
        } else {
            $('.alert-danger[err=username]', this).slideUp('100');
        }
        if (!isEmail($('#email', this).val())) {
            $('.alert-danger[err=email]', this).slideDown('100');
            ok = false;
        } else {
            $('.alert-danger[err=email]', this).slideUp('100');
        }
        if ($('#password', this).val().length < 1) {
            $('.alert-danger[err=password]', this).slideDown('100');
            ok = false;
        } else {
            $('.alert-danger[err=password]', this).slideUp('100');
            if ($('#password', this).val() != $('#password2', this).val()) {
                $('.alert-danger[err=password2]', this).slideDown('100');
                ok = false;
            } else {
                $('.alert-danger[err=password2]', this).slideUp('100');
            }
        }
        if (!ok) return;
        $('#signupModal .modal-loading').show();
        $.post("action.php?signup", {
            username: $('#username', this).val(),
            password: $('#password', this).val(),
            email: $('#email', this).val()
        }, function(data) {
            if (data.result == 'msg_ok') {
                clearModal('signup');
            } else if (data.result == 'msg_unique') {
                $('#signupModal .modal-loading').hide();
            }
        }).fail(function() {
            alert("error");
        });
    });
    $('#user_logout').click(function(e) {
        $.post("action.php?logout", {}, function(data) {
            getOut()
        }).fail(function() {
            alert("error");
        });
    })
    $('#addPledgeForm').submit(function(e) {
        e.preventDefault();
        if ($('#money', this).val() == "") {
            $('.alert-danger[err=money_wrong]', this).slideDown('100');
            return false;
        }
        $('.alert-danger[err=money_wrong]', this).slideUp('100');
        $('#addPledge .modal-loading').show();
        $.post("action.php?make-pledge", {
            money: $('#addPledgeForm #money').val(),
            id_project: $('#addPledgeForm').attr('id_project')
        }, function(data) {
            console.log(data);
            if (data.result == 'msg_ok') {
                clearModal('pledge');
            }
        }).fail(function() {
            alert("error");
        });
        $('#addPledge .modal-loading').hide();
    })
})

function guest() {
    $('.link_signin').show();
    $('.link_signup').show();
}

function getOut() {
    $('.link_signin').show();
    $('.link_signup').show();
    $('.dd_user_menu').hide();
    $('.link_admin').hide();
}

function goIn() {
    $.post("action.php?user-data", {}, function(data) {
        if (data.result != 'msg_ok') return false;
        $('.link_signin').hide();
        $('.link_signup').hide();
        $('.dd_user_menu').show();
        if (data.user.img == "")
            $('#user-img').css('background-image', 'url(/media/img/avatar.png)');
        else
            $('#user-img').css('background-image', 'url(' + data.user.img + ')');
        if (data.user.fname == "" && data.user.lname == "") {
            $('#user_fname_lname').hide();
            $('#user_fname_lname_separator').hide();
        } else {
            $('#user_fname_lname').show().text(data.user.fname + ' ' + data.user.lname);
            $('#user_fname_lname_separator').show();
        }
        if (data.user.admin == 1) {
            $('.link_admin').show();
        }
    }).fail(function() {
        alert("error");
    });
    clearModal('signin');
}

function clearModal(where) {
    if (where == 'signin') {
        $('#signInForm #username').val('');
        $('#signInForm #password').val('');
        $('#signInForm .alert-danger[err=username_password]').slideUp('100');
        $('#signInForm #rememberMe').prop('checked', false);
        $('#signinModal .modal-loading').hide();
        $('#signinModal').modal('hide');
    } else if (where == 'signup') {
        objs = ['username', 'email', 'password', 'password2'];
        for (var i = 0; i < objs.length; i++) {
            $('#signUpForm #' + objs[i]).val('');
            $('#signUpForm .alert-danger[err=' + objs[i] + ']').slideUp('100')
        }
        $('#signupModal .modal-loading').hide();
        $('#signupModal').modal('hide')
    } else if (where == 'pledge') {
        $('#addPledgeForm #money').val('');
        $('#addPledge').modal('hide');
    }
}

function ModalCloseOpen(close, open) {
    $(close).modal('hide').on('hidden.bs.modal', function(e) {
        $(open).modal('show')
    });
    $(open).on('shown.bs.modal', function() {
        $(close).off('hidden.bs.modal')
    })
    return false;
}

function CloseOpenModals() {
    $('.modal-loading').hide();
    $('#closeSignUpOpenSignIn').click(function() {
        ModalCloseOpen("#signupModal", "#signinModal");
    });
    $('#closeSignInOpenSignUp').click(function() {
        ModalCloseOpen("#signinModal", "#signupModal");
    });
    $('#closeSignInOpenResetPassword').click(function() {
        ModalCloseOpen("#signinModal", "#resetPasswordModal");
    });
    $('#closeResetPasswordOpenSignUp').click(function() {
        ModalCloseOpen("#resetPasswordModal", "#signupModal");
    });
}

function resizeToCover() {
    $('.container-video').width($(window).width());
    $('.container-video').height($(window).height());
    var scale_h = $(window).width() / vid_w_orig;
    var scale_v = $(window).height() / vid_h_orig;
    var scale = scale_h > scale_v ? scale_h : scale_v;
    if (scale * vid_w_orig < min_w) {
        scale = min_w / vid_w_orig;
    };
    $('.container-video video').width(scale * vid_w_orig);
    $('.container-video video').height(scale * vid_h_orig);
    $('.container-video').scrollLeft(($('video').width() - $(window).width()) / 2);
    $('.container-video').scrollTop(($('video').height() - $(window).height()) / 2);
};

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
var app = angular.module('backer', []);
app.controller('index', function($scope, $http) {
    $http.get('data.php?index').then(function(response) {
        $scope.intro = response.data.header;
        angular.forEach($scope.intro, function(value, key) {
            x = value.value;
            value.value = x == '0' ? false : x == '1' ? true : x
        });
    })
})
app.controller('latestProjects', function($scope, $http) {
    $http.get('data.php?projects-latest').then(function(response) {
        $scope.data = response.data.data;
        console.log($scope.data);
        angular.forEach($scope.data, function(value, key) {
            if (value.pledged < value.goal) {
                value.fundedPercent = Math.floor(value.pledged * 100 / value.goal);
                value.funded = value.fundedPercent;
            } else {
                value.funded = 100;
                value.fundedPercent = Math.floor(value.pledged * 100 / value.goal);
            }
        });
    })
});
app.directive('myactions', function() {
    return function(scope, element, attrs) {
        if (scope.$last) {

        }
    }
})
app.controller('categories', function($scope, $http) {
    $http.get('data.php?categories').then(function(response) {
        $scope.data = response.data.data;
    })
});
app.controller('popularProjects', function($scope) {
    $scope.data = [{
        id: 'p1',
        name: 'Super Project',
        img: '/media/demo/01.jpg',
        category: "Design",
        user: 'hobroker',
        description: 'A sustainable clothing line for kids created for style, comfort and play! Ethically manufactured in Austin, TX. ',
        location: 'Pyongyang',
        goal: 93700,
        pledged: 31000,
        daysLeft: 30
    }, {
        id: 'p2',
        name: 'Second Puper Project',
        img: '/media/demo/02.jpg',
        category: "Art",
        user: 'hobroker',
        description: 'A sustainable clothing line for kids created for style',
        location: 'Pyongyang',
        goal: 72300,
        pledged: 41000,
        daysLeft: 91
    }, {
        id: 'p3',
        name: 'Super Awesome Project',
        img: '/media/demo/03.jpg',
        category: "Photography",
        user: 'hobroker',
        description: 'A sustainable clothing line for kids created for style, comfort and play! Ethically manufactured in Austin, TX. ',
        location: 'Pyongyang',
        goal: 38900,
        pledged: 89000,
        daysLeft: 49
    }];
    angular.forEach($scope.data, function(value, key) {
        if (value.pledged < value.goal) {
            value.fundedPercent = Math.floor(value.pledged * 100 / value.goal);
            value.funded = value.fundedPercent;
        } else {
            value.funded = 100;
            value.fundedPercent = Math.floor(value.pledged * 100 / value.goal);
        }
    });
});
