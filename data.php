<?php
header('Content-type: application/json; charset=utf-8');
$data = array();
include 'include/db.php';
include 'global.php';
if (isset($_GET['index'])) {
    $q = $con->prepare('SELECT id_content, element, value FROM content INNER JOIN pages ON pages.id_page=content.id_page WHERE pageName="index"');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['header'] = array();
        while ($row = $result->fetch_assoc()) {
            $data['header'][$row['element']]['element'] = $row['element'];
            $data['header'][$row['element']]['value'] = $row['value'];
            $data['header'][$row['element']]['id'] = $row['id_content'];
        }
    }
} 
elseif (isset($_GET['files-images'])) {
    $data['img'] = array_filter(glob("media/img/*") , 'is_file');
} 
elseif (isset($_GET['files-images-users'])) {
    $data['img'] = array_filter(glob("media/img/users/" . $_SESSION['username'] . "/*") , 'is_file');
} 
elseif (isset($_GET['my-profile'])) {
    $q = $con->prepare('SELECT fname, lname, username, email, img FROM users WHERE id_user=?');
    $q->bind_param('i', $_SESSION['id_user']);
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $row = $result->fetch_assoc();
        $data['profile'] = $row;
    }
} 
elseif (isset($_GET['about'])) {
    $q = $con->prepare('SELECT id_content, element, value, value2 FROM content INNER JOIN pages ON pages.id_page=content.id_page WHERE pageName="about"');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
} 
elseif (isset($_GET['categories'])) {
    $q = $con->prepare('SELECT id_category, name, img FROM categories');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
} 
elseif (isset($_GET['projects'])) {
    $q = $con->prepare('SELECT COALESCE(sum(money), 0) pledged, projects.id_project, projects.name, description, projects.img, goal, date_added, date_limit, username, categories.name category FROM projects
INNER JOIN users on users.id_user=projects.id_user
INNER JOIN categories on categories.id_category=projects.id_category
LEFT JOIN pledges on pledges.id_project=projects.id_project
GROUP BY projects.id_project
');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
} 
elseif (isset($_GET['projects-mine'])) {
    $q = $con->prepare('SELECT COALESCE(sum(money), 0) pledged, projects.id_project, projects.name, description, projects.img, goal, date_added, date_limit, username, categories.name category FROM projects
INNER JOIN users on users.id_user=projects.id_user
INNER JOIN categories on categories.id_category=projects.id_category
LEFT JOIN pledges on pledges.id_project=projects.id_project
WHERE projects.id_user=?
GROUP BY projects.id_project
');
    $q->bind_param("i", $_SESSION['id_user']);
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
} 
elseif (isset($_GET['project-single'])) {
    $q = $con->prepare('SELECT id_project, name, description, img, goal, date_added, date_limit, id_user, id_category FROM projects WHERE id_project=?');
    $q->bind_param("i", $_POST['id_project']);
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $row = $result->fetch_assoc();
        $data = $row;
    }
}
elseif(isset($_GET['projects-latest'])) {
    $q = $con->prepare('SELECT 
projects.id_project,
projects.name,
concat(substring(description, 1, 250), "...") description,
projects.img,
goal,
DATEDIFF(date_limit, date_added) AS daysLeft,
username,
categories.name category,
CONCAT(fname, " ", lname) flname,
email,
users.img img_user,
(SELECT count(*) FROM pledges
INNER JOIN projects on projects.id_project=pledges.id_project) backers,
COALESCE(sum(money), 0) pledged
FROM projects
INNER JOIN categories on categories.id_category=projects.id_category
INNER JOIN users on users.id_user=projects.id_user
INNER JOIN pledges on pledges.id_project=projects.id_project
GROUP BY categories.name
ORDER BY date_added
');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
}
elseif (isset($_GET['pledges-all'])) {
    $q = $con->prepare('SELECT projects.name,
username,
money,
date_pledged,
users.id_user
FROM projects
INNER JOIN pledges on pledges.id_project = projects.id_project
INNER JOIN users on users.id_user=pledges.id_user
');
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
}
elseif (isset($_GET['pledges-mine'])) {
    $q = $con->prepare('SELECT projects.name,
username,
money,
date_pledged,
users.id_user
FROM projects
INNER JOIN pledges on pledges.id_project = projects.id_project
INNER JOIN users on users.id_user=pledges.id_user 
WHERE users.id_user=?
        ');
    $q->bind_param('i', $_SESSION['id_user']);
    $q->execute();
    if ($q) {
        $result = $q->get_result();
        $data['data'] = array();
        while ($row = $result->fetch_assoc()) {
            array_push($data['data'], $row);
        }
    }
}
echo json_encode($data, JSON_PRETTY_PRINT);
?>