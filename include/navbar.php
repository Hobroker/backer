<?php
if (empty($w)) $w = 'home';
?>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand image" href="/">Backer</a>
            </div>
            <div class="collapse navbar-collapse" id="topnav">
                <ul class="nav navbar-nav">
                    <li <?php echo ($w=='home' )? 'class="active"': ''; ?>><a href="/">Acasă</a></li>
                    <li <?php echo ($w=='discover' )? 'class="active"': ''; ?>><a href="/discover">Descoperă</a></li>
                    <li <?php echo ($w=='about' )? 'class="active"': ''; ?>><a href="/about">Despre noi</a></li>

                    <li dn class="dn special link_signin"><a href="#signinModal" data-toggle="modal">Logare</a></li>
                    <li dn class="dn special link_signup"><a href="#signupModal" data-toggle="modal">Înregistrare</a></li>
                    <li dn class="dn special link_admin"><a href="/god" target="_blank">Admin</a></li>
                </ul>
                <ul class="nav navbar-nav pull-right dd_user_menu dn" dn>
                    <li class="dropdown account">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="img bg-cover" id="user-img"></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/god/?page=profile" id="user_fname_lname">Igor Leahu</a></li>
                            <li role="separator" class="divider" id="user_fname_lname_separator"></li>
                            <li><a href="/god/?page=profile">Profil</a></li>
                            <li><a href="/god/?page=page-projects">Creează un proiect</a></li>
                            <li><a href="/god/?page=page-pledges">Proiecte susținute</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" id="user_logout">Log out</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-right" role="search" action="index.php" method="GET">
                    <div class="form-group">
                        <input type="text" id="search" name="search" class="form-control" placeholder="Search Projects">
                        <label for="search"><i class="fa fa-search fa-rotate-90"></i></label>
                    </div>
                    <button type="submit" class="btn btn-default"></button>
                </form>
            </div>
        </div>
    </nav>
