<footer class="footer" id="footer">
    <div class="footer-header">

    </div>
    <div class="footer-body">
        <div class="container">
            <ul class="nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
                <li><a href="#">Home</a></li>
            </ul>
        </div>
    </div>
</footer>

<!-- Modals -->
<div class="modal fade sign" id="signinModal">
    <div class="modal-dialog">
        <div class="modal-loading">
            <div class="modal-loading-container">
                <div class="content">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <div class="icon"><i class="fa fa-lock"></i></div>
                <h4 class="modal-title">Logare</h4>
            </div>
            <div class="modal-body">
                <form action="" method="POST" role="form" id="signInForm" class="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="username"><i class='fa fa-user'></i></label>
                            </div>
                            <input type="text" class="form-control" id="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="password"><i class='fa fa-lock'></i></label>
                            </div>
                            <input type="password" class="form-control" id="password" placeholder="Parola">
                        </div>
                    </div>
                    <div class="alert alert-danger" err="username_password">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Username sau parolă greșită!
                        </div>
                    </div>
<!--                     <p class="text-left">
                        <input type="checkbox" name="rememberMe" id="rememberMe">
                        <label for="rememberMe">Păstrează-mă logat</label>
                        <a href="#" nowhere class="f-right" id="closeSignInOpenResetPassword">Am uitat parola</a>
                    </p> -->
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <p>Nu ai cont? <a href="#" nowhere id="closeSignInOpenSignUp">Înregistrează-te</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade sign" id="signupModal">
    <div class="modal-dialog">
        <div class="modal-loading">
            <div class="modal-loading-container">
                <div class="content">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <div class="icon"><i class="fa fa-lock"></i></div>
                <h4 class="modal-title">Înregistrare</h4>
            </div>
            <div class="modal-body">
                <form action="" method="POST" role="form" id="signUpForm" class="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="username"><i class='fa fa-user'></i></label>
                            </div>
                            <input type="text" class="form-control" id="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="email"><i class='fa fa-envelope'></i></label>
                            </div>
                            <input type="email" class="form-control" id="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="password"><i class='fa fa-lock'></i></label>
                            </div>
                            <input type="password" class="form-control" id="password" placeholder="Parolă">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="password2"><i class='fa fa-lock'></i></label>
                            </div>
                            <input type="password" class="form-control" id="password2" placeholder="Confimare parolă">
                        </div>
                    </div>
                    <div class="alert alert-danger" err="username">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Introduceți un username!
                        </div>
                    </div>
                    <div class="alert alert-danger" err="email">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Introduceți un email valid!
                        </div>
                    </div>
                    <div class="alert alert-danger" err="password">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Introduceți o parolă!
                        </div>
                    </div>
                    <div class="alert alert-danger" err="password2">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Confirmați parola!
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <p>Deja ești membru? <a href="#" nowhere id="closeSignUpOpenSignIn">Loghează-te</a></p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade sign" id="resetPasswordModal">
    <div class="modal-dialog">
        <div class="modal-loading">
            <div class="modal-loading-container">
                <div class="content">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                <div class="icon"><i class="fa fa-lock"></i></div>
                <h4 class="modal-title">Resetare parolă</h4>
            </div>
            <div class="modal-body">
                <form action="" method="POST" role="form" id="resetPasswordForm" class="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="username"><i class='fa fa-user'></i></label>
                            </div>
                            <input type="text" class="form-control" id="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="alert alert-danger" err="username_password">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Username greșit!
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <p>Nu ai cont? <a href="#" nowhere id="closeResetPasswordOpenSignUp">Înregistrează-te</a></p>
            </div>
        </div>
    </div>
</div>