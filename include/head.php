<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/fonts.css">
<?php if (!isset($sub)): ?>
<link rel="stylesheet" href="/assets/css/global.css">
<link rel="stylesheet" href="/assets/css/main.css">
<link rel="stylesheet" href="/assets/css/responsive.css">
<?php else: ?>
<link rel="stylesheet" href="/assets/css/god.css">	
<?php endif ?>
