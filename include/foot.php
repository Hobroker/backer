<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/angular.min.js"></script>
<?php if (!isset($sub)): ?>
<script type="text/javascript" src="/assets/js/main.js"></script>
<?php if ($in): ?>
<script type="text/javascript">
$(document).ready(function() {
    goIn();
});
</script>
<?php else: ?>
<script type="text/javascript">
$(document).ready(function() {
    guest();
});
</script>
<?php endif;?>
<?php else: ?>
<script type="text/javascript" src="/assets/js/god.js"></script>
<?php endif ?>