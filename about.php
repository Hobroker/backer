<?php
$w = 'about';
include 'global.php';
include 'include/db.php';

$data = array();
$q = $con->prepare('SELECT element, value, value2 FROM content INNER JOIN pages ON pages.id_page=content.id_page WHERE pageName="about"');
$q->execute();
if ($q) {
    $result = $q->get_result();
    while ($row = $result->fetch_assoc()) {
        array_push($data, $row);
    }
}
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Backer</title>
<?php
include $put['head'];
?>
    </head>

    <body ng-app="backer">
<?php
include $put['navbar'];
?>
            <div class="container-fluid container-fullscreen bg-cover first" style="background-image: url('/media/img/about.jpg')">
                <div class="container-video">
                    <video id="video-background" preload muted autoplay loop>
                        <!-- autoplay loop -->
                        <source src="/media/video/about.mp4" type="video/mp4">
                        <source src="/media/video/about.ogg" type="video/ogg">
                    </video>
                </div>
                <div class="container-video-content">
                    <div class="container">
                        <h1>Misiunea noastră este să aducem viață proiectelor creative</h1>
                    </div>
                </div>
            </div>
<?php
$i = 0;
while($i < count($data)) {
    if($data[$i]['element'] == 'img'):
?>
<div class="container-fluid container-fullscreen bg-cover bg-pos-top-center" style="background-image: url('<?php echo $data[$i]["value2"]; ?>')"></div>
<?php
else:
?>
<div class="container-medium <?php echo $text_class[$data[$i]['element']]; ?>">
    <div class="content">
        <?php if($data[$i]['element'] == 'text'): ?>
        <h2 class="align-left text-normal"><?php echo $data[$i]["value"]; ?></h2>
        <p>
            <?php echo $data[$i]["value2"]; ?>
        </p>
        <?php else: ?>
        <h2>
            <?php echo $data[$i]["value2"]; ?>
        </h2>
        <?php endif; ?>
    </div>
</div>
<?php
endif;
$i++;
}
?>
            <div class="q_p q_pb-0 container-fluid container-clean" id="getStarted">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="h-section-header big text-center q_mb-small q_mt-0">Începe azi!</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-primary btn-lg btn-block dirty">Descoperă</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-primary btn-lg btn-block dirty">Creează</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
include $put['footer'];
include $put['foot'];
?>
    </body>

    </html>
