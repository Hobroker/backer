<?php
$w = 'home';
include 'global.php';
include 'include/db.php';

$data = array();
$q = $con->prepare('SELECT 
projects.id_project,
projects.name,
description,
projects.img,
goal,
DATEDIFF(date_limit, date_added) AS daysLeft,
username,
categories.name category,
CONCAT(fname, " ", lname) flname,
email,
users.img img_user,
(SELECT count(*) FROM pledges
INNER JOIN projects on projects.id_project=pledges.id_project
WHERE projects.id_project=?) backers,
sum(money) pledged
FROM projects
INNER JOIN categories on categories.id_category=projects.id_category
INNER JOIN users on users.id_user=projects.id_user
INNER JOIN pledges on pledges.id_project=projects.id_project
WHERE projects.id_project=?');
$q->bind_param("ii", $_GET['what'], $_GET['what']);
$q->execute();
if ($q) {
    $result = $q->get_result();
    $row = $result->fetch_assoc();
    $data = $row;
}

?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Backer</title>
        <?php include $put['head']; ?>
    </head>

    <body ng-app="backer" ng-controller="index">
        <?php include $put['navbar']; ?>
            <div class="container">
                <h1 class="text-center special"><?php echo $data['name']; ?> <br> <small><i>creat de către <b><?php echo $data['flname']; ?></i></b></small></h1>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="img-container">
                            <img src="<?php echo $data['img']; ?>" alt="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <h1 class="huge"><?php echo $data['backers']; ?> <br> <small><?php echo $data['backers']==1?"susținător":"susținători"; ?></small></h1>
                        <h1 class="huge"><?php echo $data['pledged']; ?> lei <br> <small>adunați</small></h1>
                        <h1 class="huge"><?php echo $data['daysLeft']; ?> <br> <small>zile rămase</small></h1>
                        <button class="btn btn-block btn-primary btn-lg" href="#addPledge" data-toggle="modal">Susține</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <h2>Descriere</h2>
                        <p class="description">
                            <?php echo nl2br($data['description']); ?>
                        </p>
                    </div>
                    <div class="col-sm-3">
                        <div class="row">
                            <div class="col-xs-10">
                                <div class="img-container special-v2">
                                    <img src="<?php echo $data['img_user'] ?>" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 straight">
                                <p><i class="fa fa-user"></i> <?php echo $data['flname']; ?></p>
                                <p><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $data['email']; ?>"><?php echo $data['email']; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include $put['footer']; include $put['foot']; ?>
    </body>

    </html>
<div class="modal fade sign special" id="addPledge">
    <div class="modal-dialog">
        <div class="modal-loading">
            <div class="modal-loading-container">
                <div class="content">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon"><i class="fa fa-paypal"></i></div>
                <h4 class="modal-title">Susține proiectul <br> <b><?php echo $data['name']; ?></b></h4>
            </div>
            <div class="modal-body">
                <form action="" method="POST" role="form" id="addPledgeForm" class="form" id_project="<?php echo $data['id_project']; ?>">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon" data-toggle="tooltip">
                                <label for="money"><i class='fa fa-money'></i></label>
                            </div>
                            <input type="text" class="form-control only-numbers" id="money" placeholder="MDL">
                        </div>
                    </div>
                    <div class="alert alert-danger" err="money_wrong">
                        <div class="alert-content">
                            <button type="button" class="close" aria-hidden="true">&times;</button>
                            <i class="fa fa-exclamation-triangle"></i> Introduceți o sumă de bani!
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">OK</button>
                </form>
            </div>
        </div>
    </div>
</div>