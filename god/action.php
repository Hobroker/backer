<?php
header('Content-type: application/json; charset=utf-8');
$data = array();
include '../include/db.php';
include '../global.php';
if (isset($_GET['index-intro']) && isset($_POST['value']) && isset($_POST['id'])) {
    $q = $con->prepare('UPDATE content SET value=? WHERE id_content=?');
    $q->bind_param("si", $_POST['value'], $_POST['id']);
    $q->execute();
} 
elseif (isset($_GET['index-block-visibility']) && isset($_POST['value']) && isset($_POST['id'])) {
    $q = $con->prepare('UPDATE content SET value=? WHERE id_content=?');
    $q->bind_param("si", $_POST['value'], $_POST['id']);
    $q->execute();
} 
elseif (isset($_GET['deleteFile']) && isset($_POST['path'])) {
    unlink($_POST['path']);
} 
elseif (isset($_GET['profile-update'])) {
    $q = $con->prepare('UPDATE users SET
fname=?,
lname=?,
email=?,
username=?,
img=?
WHERE id_user=?');
    $q->bind_param("sssssi", $_POST['fname'], $_POST['lname'], $_POST['email'], $_POST['username'], $_POST['img'], $_SESSION['id_user']);
    $q->execute();
} 
elseif (isset($_GET['about-add'])) {
    $q = $con->prepare('INSERT INTO content(element, value, value2, id_page) VALUES (?, ?, ?, 3);');
    $q->bind_param("sss", $_POST['element'], $_POST['value'], $_POST['value2']);
    $q->execute();
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    } 
    else {
        $data['result'] = 'msg_fail';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['about-add-img'])) {
    $q = $con->prepare('INSERT INTO content(element, value, value2, id_page) VALUES ("img", "img", ?, 3);');
    $q->bind_param("s", $_POST['value2']);
    $q->execute();
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    } 
    else {
        $data['result'] = 'msg_fail';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['about-edit-img'])) {
    $q = $con->prepare('UPDATE content SET value2=? WHERE id_content=?');
    $q->bind_param("ss", $_POST['value2'], $_POST['id_content']);
    $q->execute();
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    } 
    else {
        $data['result'] = 'msg_fail';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['about-edit-text'])) {
    $q = $con->prepare('UPDATE content SET element=?, value=?, value2=? WHERE id_content=?');
    $q->bind_param("sssi", $_POST['element'], $_POST['value'], $_POST['value2'], $_POST['id_content']);
    $q->execute();
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    } 
    else {
        $data['result'] = 'msg_fail';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['about-delete'])) {
    $q = $con->prepare('DELETE FROM content WHERE id_content=?');
    $q->bind_param("i", $_POST['id_content']);
    $q->execute();
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    } 
    else {
        $data['result'] = 'msg_fail';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['category-delete'])) {
    $q = $con->prepare('DELETE FROM categories WHERE id_category=?');
    $q->bind_param("i", $_POST['id_category']);
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['project-delete'])) {
    $q = $con->prepare('DELETE FROM projects WHERE id_project=?');
    $q->bind_param("i", $_POST['id_project']);
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['category-add'])) {
    $q = $con->prepare('INSERT INTO categories(name, img) VALUES (?, ?)');
    $q->bind_param("ss", $_POST['name'], $_POST['img']);
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['project-add'])) {
    $q = $con->prepare('INSERT INTO projects(name, description, id_user, id_category, img, goal, date_added, date_limit) VALUES (?, ?, ?, ?, ?, ?, now(), ?)');
    $q->bind_param("ssiisis", $name, $description, $id_user, $id_category, $img, $goal, $date_limit);
    $name = $_POST['name'];
    $description = $_POST['description'];
    $id_user = $_SESSION['id_user'];
    $id_category = $_POST['id_category'];
    $img = $_POST['img'];
    $goal = $_POST['goal'];
    $date_limit = $_POST['date_limit'];
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
}
elseif (isset($_GET['project-edit'])) {
    $q = $con->prepare('UPDATE projects SET
name=?,
description=?,
id_user=?,
id_category=?,
img=?,
goal=?,
date_limit=?
WHERE id_project=?
        ');
    $q->bind_param("ssiisisi", $name, $description, $id_user, $id_category, $img, $goal, $date_limit, $id_project);
    $name = $_POST['name'];
    $description = $_POST['description'];
    $id_user = $_SESSION['id_user'];
    $id_category = $_POST['id_category'];
    $img = $_POST['img'];
    $goal = $_POST['goal'];
    $date_limit = $_POST['date_limit'];
    $id_project = $_POST['id_project'];
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['category-edit'])) {
    $q = $con->prepare('UPDATE categories SET name=?, img=? WHERE id_category=?');
    $q->bind_param("ssi", $_POST['name'], $_POST['img'], $_POST['id_category']);
    $q->execute();
    $data['result'] = 'msg_fail';
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
    echo json_encode($data);
} 
elseif (isset($_GET['upload-img-user'])) {
    if (!in_array(strtolower($extension) , $allowed_img) && isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
        $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES['upl']['tmp_name'], '../media/img/users/' . $_SESSION['username'] . '/' . file_newname('../media/img/users/' . $_SESSION['username'] . '/', $_FILES['upl']['name']))) {
        }
    }
} 
elseif (isset($_GET['upload-img-site'])) {
    if (!in_array(strtolower($extension) , $allowed_img) && isset($_FILES['upl']) && $_FILES['upl']['error'] == 0) {
        $extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);
        if (move_uploaded_file($_FILES['upl']['tmp_name'], '../media/img/' . file_newname('../media/img/', $_FILES['upl']['name']))) {
        }
    }
} 
else {
    header('Location: index.php');
}
?>
