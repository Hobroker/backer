<div class="page-projects" id="page-projects" ng-controller="projects">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Proiecte</li>
    </ol>
    <h2>Proiecte <small></small></h2>
    <?php if($admin): ?>
        <div class="tabs-holder">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#site" aria-controls="site" role="tab" data-toggle="tab">Toate proiectele</a>
                </li>
                <li role="presentation">
                    <a href="#user" aria-controls="user" role="tab" data-toggle="tab">Proiectele mele</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="site">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="one-line-stuff v5">
                                    <button type="button" class="btn btn-primary project-item-btn">
                                        <i class="fa fa-plus"></i> Adaugă
                                    </button>
                                </div>
                                <table class="table table-bordered table-hover table-striped">
                                    <tr>
                                        <th nowrap>Nume</th>
                                        <th nowrap>Descriere</th>
                                        <th nowrap>Imagine</th>
                                        <th nowrap>Utilizator</th>
                                        <th nowrap>Categorie</th>
                                        <th nowrap>Suma necesară</th>
                                        <th nowrap>Suma adunată</th>
                                        <th nowrap>Adăugat</th>
                                        <th nowrap>Data limită</th>
                                        <th nowrap>Acțiuni</th>
                                    </tr>
                                    <tr ng-repeat="q in data_all" projects>
                                        <td>{{q.name}}</td>
                                        <td>{{q.description}}</td>
                                        <td><img class="xs-img" ng-src="{{q.img}}" alt=""></td>
                                        <td><b>{{q.username}}</b></td>
                                        <td><b>{{q.category}}</b></td>
                                        <td>{{q.goal}} lei</td>
                                        <td>{{q.pledged}} lei</td>
                                        <td>{{q.date_added}}</td>
                                        <td>{{q.date_limit}}</td>
                                        <td width="1">
                                            <div class="table-actions">
                                                <button type="button" id="{{q.id_project}}" title="Șterge" class="btn btn-danger project-delete-item"><i class="fa fa-trash"></i></button>
                                                <button type="button" id="{{q.id_project}}" title="Modifică" class="btn btn-default project-edit-item"><i class="fa fa-edit"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="user">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="one-line-stuff v5">
                                    <button type="button" class="btn btn-primary project-item-btn">
                                        <i class="fa fa-plus"></i> Adaugă
                                    </button>
                                </div>
                                <table class="table table-bordered table-hover table-striped">
                                    <tr>
                                        <th nowrap>Nume</th>
                                        <th nowrap>Descriere</th>
                                        <th nowrap>Imagine</th>
                                        <th nowrap>Utilizator</th>
                                        <th nowrap>Categorie</th>
                                        <th nowrap>Suma necesară</th>
                                        <th nowrap>Suma adunată</th>
                                        <th nowrap>Adăugat</th>
                                        <th nowrap>Data limită</th>
                                        <th nowrap>Acțiuni</th>
                                    </tr>
                                    <tr ng-repeat="q in data_user" projects>
                                        <td>{{q.name}}</td>
                                        <td>{{q.description}}</td>
                                        <td><img class="xs-img" ng-src="{{q.img}}" alt=""></td>
                                        <td><b>{{q.username}}</b></td>
                                        <td><b>{{q.category}}</b></td>
                                        <td>{{q.goal}} lei</td>
                                        <td>{{q.pledged}} lei</td>
                                        <td>{{q.date_added}}</td>
                                        <td>{{q.date_limit}}</td>
                                        <td width="1">
                                            <div class="table-actions">
                                                <button type="button" id="{{q.id_project}}" title="Șterge" class="btn btn-danger project-delete-item"><i class="fa fa-trash"></i></button>
                                                <button type="button" id="{{q.id_project}}" title="Modifică" class="btn btn-default project-edit-item"><i class="fa fa-edit"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php else: ?>
    <div class="container-fluid files">
        <div class="row">
            <div class="col-sm-12">
                <div class="one-line-stuff v5">
                    <button type="button" class="btn btn-primary project-item-btn">
                        <i class="fa fa-plus"></i> Adaugă
                    </button>
                </div>
                <table class="table table-bordered table-hover table-striped">
                    <tr>
                        <th nowrap>Nume</th>
                        <th nowrap>Descriere</th>
                        <th nowrap>Imagine</th>
                        <th nowrap>Utilizator</th>
                        <th nowrap>Categorie</th>
                        <th nowrap>Suma necesară</th>
                        <th nowrap>Suma adunată</th>
                        <th nowrap>Adăugat</th>
                        <th nowrap>Data limită</th>
                        <th nowrap>Acțiuni</th>
                    </tr>
                    <tr ng-repeat="q in data_user" projects>
                        <td>{{q.name}}</td>
                        <td>{{q.description}}</td>
                        <td><img class="xs-img" ng-src="{{q.img}}" alt=""></td>
                        <td><b>{{q.username}}</b></td>
                        <td><b>{{q.category}}</b></td>
                        <td>{{q.goal}} lei</td>
                        <td>{{q.pledged}} lei</td>
                        <td>{{q.date_added}}</td>
                        <td>{{q.date_limit}}</td>
                        <td width="1">
                            <div class="table-actions">
                                <button type="button" id="{{q.id_project}}" title="Șterge" class="btn btn-danger project-delete-item"><i class="fa fa-trash"></i></button>
                                <button type="button" id="{{q.id_project}}" title="Modifică" class="btn btn-default project-edit-item"><i class="fa fa-edit"></i></button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php endif; ?>
        </div>
        <div class="overlay-stuff" id="projectActions">
            <div class="content">
                <button type="button" class="close">&times;</button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Creare proiect
                    </div>
                    <div class="panel-body">
                        <form class="form" id="form-add-project" role="form" mode="">
                            <div class="form-group">
                                <label for="proj-name">Nume</label>
                                <input type="text" class="form-control" id="proj-name" required>
                            </div>
                            <div class="form-group">
                                <label for="proj-description">Descriere</label>
                                <textarea id="proj-description" rows="10" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="proj-category">Categorie</label>
                                <select id="proj-category" class="form-control full" ng-controller="categories">
                                    <option value="0">Alege</option>
                                    <option value="{{q.id_category}}" ng-repeat="q in data">{{q.name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="proj-goal">Suma de bani necesară</label>
                                <div class="input-group">
                                    <div class="input-group-addon">MDL</div>
                                    <input type="text" class="form-control only-numbers" id="proj-goal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="proj-date_limit">Data limită</label>
                                <input type="text" class="form-control date-picker" id="proj-date_limit">
                            </div>
                            <div class="one-line-stuff">
                                <input type="hidden" id="proj-img">
                                <button type="button" class="btn btn-primary select-img select-img-user"><i class="fa fa-search"></i> Caută</button>
                            </div>
                            <div class="form-group">
                                <div class="img-container">
                                    <img src="" alt="" id="proj-img-actual">
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block">OK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="project-delete">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Ștergeți acest proiect?</h3>
                        </div>
                        <div class="modal-body">
                            El nu va putea fi recuperat!
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-danger btn-block" data-dismiss="modal" q-action='y'>Da</button>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal" q-action='n'>Nu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
