<div class="modal fade" id="modalSuccess">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Succes</h3>
                </div>
                <div class="modal-body">
                    Modificările au fost efectuate
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                            <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade fail" id="modalError">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Eroare</h3>
                </div>
                <div class="modal-body">
                    Modificările nu au fost efectuate
                    <br> Bla bla bla
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>