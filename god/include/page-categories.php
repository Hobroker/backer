<div class="page-categories" id="page-categories" ng-controller="categories">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Categorii</li>
    </ol>
    <h2>Categorii <small></small></h2>
    <div class="panel panel-default">
        <div class="panel-heading">
            Categoriile de proiecte
        </div>
        <div class="panel-body">
            <div class="one-line-stuff v2">
                <button type="button" class="btn btn-primary category-item-btn" myAction='add'>
                    <i class="fa fa-upload"></i> Adaugă
                </button>
            </div>
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th nowrap>Nume categorie</th>
                    <th nowrap>Imagine</th>
                    <th nowrap>Acțiuni</th>
                </tr>
                <tr ng-repeat="q in data" categories>
                    <td data="name">{{q.name}}</td>
                    <td>
                        <img data="img" class="xs-img" ng-src="{{q.img}}" alt="">
                    </td>
                    <td width="1">
                        <div class="table-actions">
                            <button type="button" id="{{q.id_category}}" title="Șterge" class="btn btn-danger category-delete-item"><i class="fa fa-trash"></i></button>
                            <button type="button" id="{{q.id_category}}" title="Modifică" class="btn btn-default category-edit-item"><i class="fa fa-edit"></i></button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="overlay-stuff" id="categoryActions">
    <div class="content">
        <button type="button" class="close">&times;</button>
        <div class="panel panel-default">
            <div class="panel-heading">
                Adăugă categorie
            </div>
            <div class="panel-body">
                <form class="form" id="form-add-category" role="form">
                    <div class="form-group">
                        <label for="cat-name">Nume categorie</label>
                        <input type="text" class="form-control" id="cat-name" required>
                    </div>
                    <div class="form-group">
                        <div class="one-line-stuff">
                            <label for="cat-img">Imagine</label>
                            <input type="hidden" id="cat-img">
                            <button type="button" class="btn btn-primary pull-right select-img">Caută</button>
                        </div>
                        <div class="img-container">
                            <img alt="">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-block" type="submit">Adaugă</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="category-delete">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Ștergeți această categorie?</h3>
                </div>
                <div class="modal-body">
                    Ea nu va putea fi recuperată!
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal" q-action='y'>Da</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-primary btn-block" data-dismiss="modal" q-action='n'>Nu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>