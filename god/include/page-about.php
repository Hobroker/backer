<div class="page-about" id="page-about" ng-controller="about">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Pagini</li>
        <li class="active">Despre noi</li>
    </ol>
    <h2>Despre noi <small></small></h2>
    <div class="panel panel-default">
        <div class="panel-heading">
            Conținutul paginii 'Despre noi'
        </div>
        <div class="panel-body">
            <div class="one-line-stuff v2">
                <button type="button" class="btn btn-primary about-item-btn" data-toggle="modal" myAction='add' data-target="#aboutItem">
                    <i class="fa fa-upload"></i> Adaugă
                </button>
            </div>
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <th nowrap>Titlu <small>(pentru text)</small></th>
                    <th nowrap>Conținut</th>
                    <th nowrap>Tip</th>
                    <th nowrap>Acțiuni</th>
                </tr>
                <tr ng-repeat="q in data" dabout>
                    <td q-data="value">{{q.value}}</td>
                    <td q-data="value2">
                        <img class="xs-img" ng-src="{{q.value2}}" alt="{{q.value2}}" ng-if="q.value == 'img'">
                        <span ng-if="q.value != 'img'">{{q.value2}}</span>
                    </td>
                    <td q-data="element" element-original="{{q.element}}" nowrap>{{q.about_types}}</td>
                    <td>
                        <div class="table-actions">
                            <button type="button" id="{{q.id_content}}" title="Șterge" class="btn btn-danger about-delete-item"><i class="fa fa-trash"></i></button>
                            <button type="button" id="{{q.id_content}}" value="{{q.value}}" value2="{{q.value2}}" title="Modifică" class="btn btn-default about-edit-item"><i class="fa fa-edit"></i></button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="aboutItem">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Adăugare conținut pe pagina <b>Despre noi</b></h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="aboutContentType">Tip conținut</label>
                        <select class="form-control full" id="aboutItemTypeSelect">
                            <option value="text" selected>Text</option>
                            <option value="img">Imagine</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="panel panel-default about-text-panel dn" dn>
                        <div class="panel-heading">Conținut</div>
                        <div class="panel-body">
                            <form class="form" role="form" id="about-form-text">
                                <div class="form-group">
                                    <label for="aboutContentType">Tip text</label>
                                    <select class="form-control full" id="aboutContentTypeText">
                                        <option value="text">Text simplu</option>
                                        <option value="c_accent">Background Roșu</option>
                                        <option value="c_primary">Background Albastru</option>
                                        <option value="c_text">Background Negru</option>
                                    </select>
                                    <div class="form-group">
                                        <label for="aboutContentTitle">Titlu</label>
                                        <input id="aboutContentTitle" type="text" class="form-control" value="{{index.message_text_2.value}}" id="message_text_2" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="aboutContentContent">Conținut</label>
                                        <textarea id="aboutContentContent" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">OK</button>
                            </form>
                        </div>
                    </div>
                    <div class="panel panel-default about-img-panel dn" dn>
                        <div class="panel-heading">Imagine</div>
                        <div class="panel-body">
                            <form class="form" role="form" id="about-form-img">
                                <div class="one-line-stuff">
                                    <input type="hidden" id="about-img-src">
                                    <button type="button" class="btn btn-primary select-img"><i class="fa fa-search"></i> Caută</button>
                                </div>
                                <div class="form-group">
                                    <div class="img-container">
                                        <img src="" alt="">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">OK</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="aboutEditImg">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Modificare imagine</h3>
                </div>
                <div class="modal-body">
                    <form class="form" role="form" id="about-form-img-edit">
                        <div class="one-line-stuff">
                            <input type="hidden" id="about-img-edit-src">
                            <button type="button" class="btn btn-primary select-img"><i class="fa fa-search"></i> Caută</button>
                        </div>
                        <div class="form-group">
                            <div class="img-container">
                                <img src="" alt="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">OK</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="aboutEditText">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Modificare text</h3>
                </div>
                <div class="modal-body">
                    <form class="form" role="form" id="about-form-text-edit">
                        <div class="form-group">
                            <label for="aboutContentType">Tip text</label>
                            <select class="form-control full" id="aboutContentTypeText">
                                <option value="text">Text simplu</option>
                                <option value="c_accent">Background Roșu</option>
                                <option value="c_primary">Background Albastru</option>
                                <option value="c_text">Background Negru</option>
                            </select>
                            <div class="form-group">
                                <label for="aboutContentTitle">Titlu</label>
                                <input id="aboutContentTitle" type="text" class="form-control" value="{{index.message_text_2.value}}" id="message_text_2" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="aboutContentContent">Conținut</label>
                                <textarea id="aboutContentContent" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">OK</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="about-delete">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title">Ștergeți acest item?</h3>
                </div>
                <div class="modal-body">
                    El nu va putea fi recuperat!
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal" q-action='y'>Da</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-primary btn-block" data-dismiss="modal" q-action='n'>Nu</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
