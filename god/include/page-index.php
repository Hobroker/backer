<div class="page-index" ng-controller="index">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Pagini</li>
        <li class="active">Acasă</li>
    </ol>
    <h2>Acasă <small></small></h2>
    <div class="panel panel-default">
        <div class="panel-heading">
            Vizibilitatea blocurilor de informații
        </div>
        <div class="panel-body">
            <form class="form" role="form" id="index-block-visibility">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="intro" ng-checked=index.intro.value> Mesaje informative (Header)
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="latestProjects" ng-checked=index.latestProjects.value> Ultimele Proiecte
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="categories" ng-checked=index.categories.value> Categorii
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="popularProjects" ng-checked=index.popularProjects.value> Cele mai populare proiecte
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvează</button>
                            <button type="Reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            Mesaje informative (Header)
        </div>
        <div class="panel-body">
            <form class="form" role="form" id="index-intro">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="message_text_1">Primul mesaj</label>
                                <input type="text" class="form-control" value="{{index.message_text_1.value}}" id="message_text_1" autocomplete="off">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="button_text_1">Nume buton</label>
                                        <input type="text" class="form-control" value="{{index.button_text_1.value}}" id="button_text_1" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="button_link_1">Adresă buton</label>
                                        <input type="text" class="form-control" value="{{index.button_link_1.value}}" id="button_link_1" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="one-line-stuff">
                                            <label for="intro_img_1">Imagine</label>
                                            <input type="hidden" id="intro_img_1" value="{{index.intro_img_1.value}}">
                                            <button type="button" class="btn btn-primary pull-right select-img">Caută</button>
                                        </div>
                                        <div class="img-container">
                                            <img ng-src="{{index.intro_img_1.value}}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="message_text_2">Al doilea mesaj</label>
                                <input type="text" class="form-control" value="{{index.message_text_2.value}}" id="message_text_2" autocomplete="off">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="button_text_2">Nume buton</label>
                                        <input type="text" class="form-control" value="{{index.button_text_2.value}}" id="button_text_2" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="button_link_2">Adresă buton</label>
                                        <input type="text" class="form-control" value="{{index.button_link_2.value}}" id="button_link_2" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="one-line-stuff">
                                            <label for="intro_img_2">Imagine</label>
                                            <input type="hidden" id="intro_img_2" value="{{index.intro_img_2.value}}">
                                            <button type="button" class="btn btn-primary pull-right select-img">Caută</button>
                                        </div>
                                        <div class="img-container">
                                            <img ng-src="{{index.intro_img_2.value}}" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary">Salvează</button>
                            <button type="Reset" class="btn btn-default">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
