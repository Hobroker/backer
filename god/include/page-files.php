<div class="page-files" id="page-files" ng-controller="files">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Imagini</li>
    </ol>
    <h2>Imagini <small></small></h2>
    <?php if($admin): ?>
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#site-img" aria-controls="site-img" role="tab" data-toggle="tab">Imaginile site-ului</a>
                </li>
                <li role="presentation">
                    <a href="#user-img" aria-controls="user-img" role="tab" data-toggle="tab">Imaginile mele</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="site-img">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="one-line-stuff v4">
                                    <button type="button" class="btn btn-primary btn-upload-img" data-toggle="modal" data-target="#imgUpload" upload-where="site">
                                        <i class="fa fa-upload"></i> Adaugă
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-lg-2 col-md-3 col-sm-4" ng-repeat="q in data" imgactions>
                                <div class="img-container">
                                    <img ng-src="../{{q}}" class="img-responsive">
                                </div>
                                <div class="actions">
                                    <button title="Șterge" class="btn btn-default q_btn-sm delete"><i class="fa fa-trash"></i></button>
                                    <button data-original-title="/{{q}}" data-toggle="tooltip" data-placement="top" class="btn btn-default q_btn-sm info"><i class="fa fa-question-circle"></i></button>
                                    <button title="Zoom" class="btn btn-default q_btn-sm zoom"><i class="fa fa-search-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="user-img">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="one-line-stuff v4">
                                    <button type="button" class="btn btn-primary btn-upload-img" data-toggle="modal" data-target="#imgUpload" upload-where="user">
                                        <i class="fa fa-upload"></i> Adaugă
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-lg-2 col-md-3 col-sm-4" ng-repeat="q in data2" imgzoom>
                                <div class="img-container">
                                    <img ng-src="../{{q}}" class="img-responsive">
                                </div>
                                <div class="actions">
                                    <button title="Șterge" class="btn btn-default q_btn-sm delete"><i class="fa fa-trash"></i></button>
                                    <button data-original-title="/{{q}}" data-toggle="tooltip" data-placement="top" class="btn btn-default q_btn-sm info"><i class="fa fa-question-circle"></i></button>
                                    <button title="Zoom" class="btn btn-default q_btn-sm zoom"><i class="fa fa-search-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php else: ?>
    <div class="container-fluid files">
        <div class="row">
            <div class="col-sm-12">
                <div class="one-line-stuff v3">
                    <button type="button" class="btn btn-primary btn-upload-img" data-toggle="modal" data-target="#imgUpload" upload-where="user">
                        <i class="fa fa-upload"></i> Adaugă
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-lg-2 col-md-3 col-sm-4" ng-repeat="q in data2" imgzoom>
                <div class="img-container">
                    <img ng-src="../{{q}}" class="img-responsive">
                </div>
                <div class="actions">
                    <button title="Șterge" class="btn btn-default q_btn-sm delete"><i class="fa fa-trash"></i></button>
                    <button data-original-title="/{{q}}" data-toggle="tooltip" data-placement="top" class="btn btn-default q_btn-sm info"><i class="fa fa-question-circle"></i></button>
                    <button title="Zoom" class="btn btn-default q_btn-sm zoom"><i class="fa fa-search-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
        <div class="modal fade full" id="imgZoom">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center modal-lg" data-dismiss="modal">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="img-container">
                                <img ng-src="../media/demo/12.jpg" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="modal fade" id="imgDelete">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Ștergeți imaginea?</h3>
                        </div>
                        <div class="modal-body">
                            Ea nu va putea fi recuperată!
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-danger btn-block" data-dismiss="modal" q-action='y'>Da</button>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal" q-action='n'>Nu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="imgUpload">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Adăugare imagini</h3>
                        </div>
                        <div class="modal-body">
                            <form class="upload form-upload-img" class="form" method="post" action="" enctype="multipart/form-data">
                                <div class="drop">
                                    <a class="btn btn-primary search-img">Caută</a>
                                    <input type="file" name="upl" multiple />
                                </div>
                                <div class="progresses">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="invisible-stuff hidden">
            <div class="container-fluid img-progress-bar">
                <div class="row">
                    <div class="col-md-6">
                        <div class="file-name">
                            xx.jpgxx.jpgxx.jpgxx.jpgxx.jpg
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="progress">
                            <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                69%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
