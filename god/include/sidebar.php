<div class="sidebar" data-spy="affix" data-offset-top="60">
    <ul class="nav">
        <?php if($admin): ?>
        <li><a href="?page=dashboard">Principala</a></li>
        <?php endif; ?>
        <li><a href="?page=profile">Profilul meu</a></li>
        <?php if($admin): ?>
        <li>
            <a href="#pages" class="collapsed special" data-toggle="collapse">Pagini <i class="fa fa-caret-left"></i></a>
            <div class="collapse nav submenu" id="pages">
                <ul class="nav">
                    <li>
                        <a href="?page=page-index">Acasă</a>
                        <a href="?page=page-about">Despre noi</a>
                    </li>
                </ul>
            </div>
        </li>
        <?php endif; ?>
        <li><a href="?page=page-files&type=img">Imagini</a></li>
        <?php if($admin): ?>
        <li><a href="?page=page-categories">Categorii</a></li>
        <?php endif; ?>
        <?php if($admin): ?>
        <li><a href="?page=page-projects">Proiecte</a></li>
        <?php else: ?>
        <li><a href="?page=page-projects">Proiectele mele</a></li>
        <?php endif; ?>
        <li><a href="?page=page-pledges">Proiecte susținute</a></li>
    </ul>
</div>
