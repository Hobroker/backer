<div class="page-pledges" id="page-pledges" ng-controller="pledges">
    <ol class="breadcrumb">
        <li class="active">Admin</li>
        <li class="active">Proiecte susținute</li>
    </ol>
    <h2>Proiecte susținute <small></small></h2>
    <?php if($admin): ?>
        <div class="tabs-holder">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#site" aria-controls="site" role="tab" data-toggle="tab">Toate proiectele</a>
                </li>
                <li role="presentation">
                    <a href="#user" aria-controls="user" role="tab" data-toggle="tab">Proiectele mele</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="site">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="single table table-bordered table-hover table-striped">
                                    <tr>
                                        <th nowrap>Nume Proiect</th>
                                        <th nowrap>Utilizator</th>
                                        <th nowrap>Suma de bani (MDL)</th>
                                        <th>Data / ora efectuării tranzacției</th>
                                    </tr>
                                    <tr ng-repeat="q in data_all">
                                        <td>{{q.name}}</td>
                                        <td>{{q.username}}</td>
                                        <td>{{q.money}}</td>
                                        <td>{{q.date_pledged}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="user">
                    <div class="container-fluid files">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="single table table-bordered table-hover table-striped">
                                    <tr>
                                        <th nowrap>Nume Proiect</th>
                                        <th nowrap>Utilizator</th>
                                        <th nowrap>Suma de bani (MDL)</th>
                                        <th>Data / ora efectuării tranzacției</th>
                                    </tr>
                                    <tr ng-repeat="q in data_mine">
                                        <td>{{q.name}}</td>
                                        <td>{{q.username}}</td>
                                        <td>{{q.money}}</td>
                                        <td>{{q.date_pledged}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php else: ?>
    <div class="container-fluid files">
        <div class="row">
            <div class="col-sm-12">
                <table class="single table table-bordered table-hover table-striped">
                    <tr>
                        <th nowrap>Nume Proiect</th>
                        <th nowrap>Utilizator</th>
                        <th nowrap>Suma de bani (MDL)</th>
                        <th>Data / ora efectuării tranzacției</th>
                    </tr>
                    <tr ng-repeat="q in data_mine">
                        <td>{{q.name}}</td>
                        <td>{{q.username}}</td>
                        <td>{{q.money}}</td>
                        <td>{{q.date_pledged}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php endif; ?>
        </div>
        <div class="overlay-stuff big" id="projectActions">
            <div class="content">
                <button type="button" class="close">&times;</button>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Creare proiect
                    </div>
                    <div class="panel-body">
                        <form class="form" id="form-add-project" role="form" mode="">
                            <div class="form-group">
                                <label for="proj-name">Nume</label>
                                <input type="text" class="form-control" id="proj-name" required>
                            </div>
                            <div class="form-group">
                                <label for="proj-description">Descriere</label>
                                <textarea id="proj-description" rows="10" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="proj-category">Categorie</label>
                                <select id="proj-category" class="form-control full" ng-controller="categories">
                                    <option value="0">Alege</option>
                                    <option value="{{q.id_category}}" ng-repeat="q in data">{{q.name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="proj-goal">Suma de bani necesară</label>
                                <div class="input-group">
                                    <div class="input-group-addon">MDL</div>
                                    <input type="text" class="form-control only-numbers" id="proj-goal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="proj-date_limit">Data limită</label>
                                <input type="text" class="form-control date-picker" id="proj-date_limit">
                            </div>
                            <div class="one-line-stuff">
                                <input type="hidden" id="proj-img">
                                <button type="button" class="btn btn-primary select-img select-img-user"><i class="fa fa-search"></i> Caută</button>
                            </div>
                            <div class="form-group">
                                <div class="img-container">
                                    <img src="" alt="" id="proj-img-actual">
                                </div>
                            </div>
                            <button class="btn btn-primary btn-block">OK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
