<?php
// protect_god();
?>
    <div class="page-index" ng-controller="my-profile">
        <ol class="breadcrumb">
            <li class="active">Admin</li>
            <li class="active">Profilul meu</li>
        </ol>
        <h2>Profilul meu <small></small></h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                Modificarea informațiilor personale
            </div>
            <div class="panel-body">
                <form class="form" role="form" id="profile-edit">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="fname">Prenume</label>
                                    <input type="text" class="form-control" id="fname" value="{{data.fname}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="lname">Nume</label>
                                    <input type="text" class="form-control" id="lname" value="{{data.lname}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" value="{{data.username}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" value="{{data.email}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="one-line-stuff">
                                        <label for="img-user">Imagine</label>
                                        <input type="hidden" id="img-user" value="{{data.img}}">
                                        <button type="button" class="btn btn-primary pull-right select-img select-img-user">Caută</button>
                                    </div>
                                    <div class="img-container">
                                        <img ng-src="{{data.img}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary">Salvează</button>
                                <button type="Reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
