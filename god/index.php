<?php
$sub = 0;
include '../global.php';
$mkay = array(
    'dashboard-admin',
    'dashboard-user',
    'page-about',
    'page-categories',
    'page-files',
    'page-index',
    'page-projects',
    'profile',
    'page-pledges'
);
if (!isset($_GET['page']) || !in_array($_GET['page'], $mkay)) {
    header('Location: ?page=dashboard-admin');
}
protect_god();
$p = $_GET['page'] . '.php';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Admin | Backer</title>
        <?php
include $put['head'];
?>
    </head>

    <body ng-app="backer">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="sidebar-toggle navbar-toggle collapsed">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand" href="#">
                    <a href="?page-dashboard"><span class="color-background">
                        <?php echo $admin?'Admin':'Panou administrare'; ?>
                    </span></a>
                    <a href="/" class="sub" target="_blank"><span class="color-accent"><i class="fa fa-external-link"></i> Acasă</span></a>
                    </span>
                </div>
                <div class="hidden">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Link</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid core">
            <div class="row page">
                <?php
include 'include/sidebar.php';
?>
                    <div class="col-md-12 container-data">
                        <?php
include "include/$p";
?>
                    </div>
            </div>
        </div>
        <div class="modal fade" id="modalSuccess">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Succes</h3>
                        </div>
                        <div class="modal-body">
                            Modificările au fost efectuate
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                                    <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade fail" id="modalError">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Eroare</h3>
                        </div>
                        <div class="modal-body">
                            Modificările nu au fost efectuate
                            <br> Bla bla bla
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12 pull-left">
                                    <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="selectImage">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Alege imaginea</h3>
                        </div>
                        <div class="modal-body" ng-controller="files">
                            <div class="container-fluid files">
                                <div class="row" what="data">
                                    <div class="col col-lg-2 col-md-3 col-sm-4" ng-repeat="q in data" imgactions>
                                        <div class="img-container">
                                            <img ng-src="/{{q}}" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" what="data2">
                                    <div class="col col-lg-2 col-md-3 col-sm-4" ng-repeat="q in data2" imgactions>
                                        <div class="img-container">
                                            <img ng-src="/{{q}}" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
include $put['foot'];
?>
            <div id="loading">
                <div class="loading-container">
                    <div class="content">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                </div>
            </div>
    </body>

    </html>
