<?php
$w = 'discover';
include 'global.php';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Backer</title>
        <?php include $put['head']; ?>
    </head>

    <body ng-app="backer">
        <?php include $put['navbar']; ?>
            <div class="container q_pt-0" ng-controller="latestProjects" id="latestProjects">
                <div id="carousel_one" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item" ng-repeat="x in data">
                            <div class="row q_todayProject">
                                <div class="col-sm-5 col-eq col-img">
                                    <a href="#">
                                        <div class="q_full-img" style="background-image: url('{{x.img}}')"></div>
                                    </a>
                                </div>
                                <div class="col-sm-7 col-eq col-info">
                                    <div class="projectDetails">
                                        <a href="#"><h4>{{x.name}}</h4></a>
                                        <a href="#" class="sub">by {{x.user}}</a>
                                        <p>{{x.description}}</p>
                                    </div>
                                    <div class="projectDetails put-bottom">
                                        <div class="info">
                                            <ul class="list">
                                                <li><a href="#"><i class="fa fa-tag fa-rotate-90"></i> {{x.category}}</a></li>
                                                <li><a href="#"><i class="fa fa-map-marker"></i> {{x.location}}</a></li>
                                            </ul>
                                        </div>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{x.funded}}" aria-valuemin="0" aria-valuemax="100" style="width: {{x.funded}}%;">
                                            </div>
                                        </div>
                                        <ul class="list">
                                            <li><span class="bold">{{x.fundedPercent}}%</span><span class="text">funded</span></li>
                                            <li><span class="bold">${{x.goal}}</span><span class="text">goal</span></li>
                                            <li><span class="bold">${{x.pledged}}</span><span class="text">pledged</span></li>
                                            <li><span class="bold">{{x.backers}}</span><span class="text">backers</span></li>
                                            <li><span class="bold">{{x.daysLeft}}</span><span class="text">days to go</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel_one" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel_one" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <?php include $put['footer']; include $put['foot']; ?>
    </body>

    </html>
