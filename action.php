<?php
header('Content-type: application/json; charset=utf-8');
$data = array();
include 'include/db.php';
include 'global.php';

session_name('login');

if (isset($_GET['deleteall'])) {
    $q = $con->prepare('DELETE FROM users');
    $q->execute();
    header('Location: test.php');
} 
elseif (isset($_GET['signup'])) {
    $q = $con->prepare('INSERT INTO users (username, email, password) VALUES (?, ?, ?)');
    $q->bind_param("sss", $username, $email, $password);
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = hash_psw($_POST['password']);
    $q->execute();
    if ($q) {
        if ($q->affected_rows != 1) {
            $data['result'] = 'msg_unique';
        } 
        else {
            mkdir('media/img/users/' . $username);
            $data['result'] = "msg_ok";
        }
    } 
    else {
        $data['result'] = "msg_fail";
    }
} 
elseif (isset($_GET['signin'])) {
    if (isset($_POST['username']) && isset($_POST['password']) && strlen($_POST['username']) > 0 && strlen($_POST['password']) > 0) {
        $q = $con->prepare('SELECT * FROM users WHERE username=? AND password=?');
        $q->bind_param("ss", $username, $password);
        $username = $_POST['username'];
        $password = hash_psw($_POST['password']);
        $q->execute();
        $result = $q->get_result();
        $ok = $result->num_rows;
        if ($q && $ok == 1) {
            $row = $result->fetch_assoc();
            $_SESSION['id_user'] = $row['id_user'];
            $_SESSION['admin'] = $row['admin'];
            $_SESSION['username'] = $row['username'];
            $data['result'] = "msg_ok";
        } 
        else {
            $data['result'] = "msg_fail";
        }
    } 
    else {
        $data['result'] = "msg_fail";
    }
} 
elseif (isset($_GET['user-data'])) {
    $q = $con->prepare('SELECT * FROM users WHERE id_user=?');
    $q->bind_param('i', $_SESSION['id_user']);
    $q->execute();
    $result = $q->get_result();
    $ok = $result->num_rows;
    if ($q && $ok == 1) {
        $row = $result->fetch_assoc();
        $data['result'] = "msg_ok";
        $data['user'] = $row;
    } 
    else {
        $data['result'] = "msg_fail";
    }
} 
elseif (isset($_GET['make-pledge'])) {
    $q = $con->prepare('INSERT INTO pledges(id_user, id_project, date_pledged, money) VALUES (?, ?, now(), ?);');
    $q->bind_param("iii", $_SESSION['id_user'], $_POST['id_project'], $_POST['money']);
    $q->execute();
    $data['result'] = "msg_fail";
    if ($q && $q->affected_rows == 1) {
        $data['result'] = 'msg_ok';
    }
} 
elseif (isset($_GET['logout'])) {
    $_SESSION = array();
    session_destroy();
} 
elseif (isset($_GET['text'])) {
    // $username = 'hobbit';
    
    
}
echo json_encode($data);

function hash_psw($what) {
    return md5($what . '_backer');
}
?>
