-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2015 at 02:59 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `backer`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id_category` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `img` varchar(256) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id_category`, `name`, `img`) VALUES
(1, 'Design', '/media/img/about2.jpg'),
(4, 'Technology', '/media/img/18.jpg'),
(5, 'Technology', '/media/img/06.jpg'),
(8, 'Food', '/media/img/14.jpg'),
(10, 'what', '/media/img/19.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id_content` int(4) NOT NULL AUTO_INCREMENT,
  `element` varchar(128) CHARACTER SET utf8 NOT NULL,
  `value` text CHARACTER SET utf8 NOT NULL,
  `id_page` int(11) NOT NULL,
  `value2` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_content`),
  KEY `id_page` (`id_page`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id_content`, `element`, `value`, `id_page`, `value2`) VALUES
(1, 'message_text_1', 'SusÈ›ine pe alÈ›ii', 2, ''),
(2, 'message_text_2', 'Fii susÈ›inut', 2, ''),
(5, 'button_text_1', 'DescoperÄƒ', 2, ''),
(6, 'button_text_2', 'CreazÄƒ', 2, ''),
(9, 'button_link_1', 'discover', 2, ''),
(10, 'button_link_2', 'create', 2, ''),
(15, 'intro', '1', 2, ''),
(16, 'latestProjects', '1', 2, ''),
(17, 'categories', '1', 2, ''),
(18, 'popularProjects', '1', 2, ''),
(19, 'intro_img_1', '/media/img/intro-2.jpg', 2, ''),
(20, 'intro_img_2', '/media/img/intro-1.jpg', 2, ''),
(44, 'c_accent', '', 3, 'Backer ajutÄƒ artiÈ™tii, muzicienii, cineaÈ™tii, designerii, È™i alÈ›i creatori sÄƒ gÄƒseascÄƒ resursele È™i sprijinul de care au nevoie pentru a realiza ideile lor. PÃ¢nÄƒ Ã®n prezent, zeci de mii de proiecte creative â€” mici È™i mari â€” au fost create, cu sprijinul comunitÄƒÈ›ii Backer\n                    '),
(45, 'text', 'Comunitatea noastrÄƒ', 3, 'Backer is an enormous global community built around creativity and creative projects. Over 9 million people, from every continent on earth, have backed a Backer project.\n\nSome of those projects come from influential artists like De La Soul or Marina AbramoviÄ‡. Most come from amazing creative people you probably havenâ€™t heard of â€” from Grandma Pearl to indie filmmakers to the band down the street.\n\nEvery artist, filmmaker, designer, developer, and creator on Backer has complete creative control over their work â€” and the opportunity to share it with a vibrant community of backers.'),
(46, 'img', 'img', 3, '/media/img/about2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id_page` int(2) NOT NULL AUTO_INCREMENT,
  `pageName` varchar(128) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_page`),
  UNIQUE KEY `pageName` (`pageName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id_page`, `pageName`) VALUES
(3, 'about'),
(2, 'index');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id_project` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `img` varchar(256) CHARACTER SET utf8 NOT NULL,
  `goal` int(10) NOT NULL,
  `date_added` date NOT NULL,
  `date_limit` date NOT NULL,
  PRIMARY KEY (`id_project`),
  KEY `id_user` (`id_user`),
  KEY `id_category` (`id_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id_project`, `name`, `description`, `id_user`, `id_category`, `img`, `goal`, `date_added`, `date_limit`) VALUES
(6, 'new ', 'dakdmaskldnaslkn kland asnasdnflaksdnfas', 65, 1, '/media/img/users/hobroker/about.jpg', 185500, '2015-12-13', '2015-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `fname` varchar(128) DEFAULT '',
  `lname` varchar(128) DEFAULT '',
  `admin` enum('1','0') NOT NULL DEFAULT '0',
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `email`, `password`, `fname`, `lname`, `admin`, `img`) VALUES
(65, 'hobroker', 'igor.leahu24@gmail.com', 'b603a2f8f11fac8f5f78be66756c78a2', 'Igor', 'Leahu', '1', '/media/img/users/hobroker/Autumnal_Wolf.jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`id_page`) REFERENCES `pages` (`id_page`);

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id_category`),
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
