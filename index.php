<?php
$w = 'home';
include 'global.php';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Backer</title>
        <?php include $put['head']; ?>
    </head>

    <body ng-app="backer" ng-controller="index">
        <?php include $put['navbar']; ?>
            <div class="container-fluid intro" ng-show="intro.intro.value">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="q_image" style="background-image: url('{{intro.intro_img_1.value}}')"></div>
                        <div class="title">
                            <h1>{{intro.message_text_1.value}}</h1>
                            <div class="text-center q_btn">
                                <a href="{{intro.button_link_1.value}}" class="btn btn-default">{{intro.button_text_1.value}}</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="q_image" style="background-image: url('{{intro.intro_img_2.value}}')"></div>
                        <div class="title">
                            <h1>{{intro.message_text_2.value}}</h1>
                            <div class="text-center q_btn">
                                <a href="{{intro.button_link_2.value}}" class="btn btn-default">{{intro.button_text_2.value}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid container-color" ng-controller="latestProjects" id="latestProjects" ng-show="intro.latestProjects.value">
                <div class="container">
                    <div class="row row-content">
                        <div class="col-md-10 col-xs-12">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade" ng-repeat="x in data" id="{{x.id_project}}" ng-class="{'active in': $first}" >
                                    <div class="row q_todayProject">
                                        <div class="col-xs-12 col-title">
                                            <h3>{{x.category}} <a href="#">Exploră categoria {{x.category}}</a></h3>
                                        </div>
                                        <div class="col-sm-5 col-eq col-img">
                                            <div class="q_full-img" style="background-image: url('{{x.img}}')"></div>
                                        </div>
                                        <div class="col-sm-7 col-eq col-info">
                                            <div class="projectDetails">
                                                <h4><a href="/project.php?what={{x.id_project}}">{{x.name}}</a></h4>
                                                <a href="#" class="sub">by {{x.username}}</a>
                                                <p>{{x.description}}</p>
                                            </div>
                                            <div class="projectDetails put-bottom">
                                                <div class="info">
                                                    <ul class="list">
                                                        <li><a href="#"><i class="fa fa-tag fa-rotate-90"></i> {{x.category}}</a></li>
                                                        <li><a href="#"><i class="fa fa-map-marker"></i> {{x.location}}</a></li>
                                                    </ul>
                                                </div>
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="{{x.funded}}" aria-valuemin="0" aria-valuemax="100" style="width: {{x.funded}}%;">
                                                    </div>
                                                </div>
                                                <ul class="list">
                                                    <li><span class="bold">{{x.fundedPercent}}%</span><span class="text">fondat</span></li>
                                                    <li><span class="bold">{{x.goal}} lei</span><span class="text">necesari</span></li>
                                                    <li><span class="bold">{{x.pledged}} lei</span><span class="text">adunați</span></li>
                                                    <li><span class="bold">{{x.backers}}</span><span class="text">susținători</span></li>
                                                    <li><span class="bold">{{x.daysLeft}}</span><span class="text">zile rămase</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 hidden-sm hidden-xs">
                            <ul class="nav nav-tabs select-first" role="tablist">
                                <li role="presentation" ng-class="{'active': $first}" ng-repeat="x in data">
                                    <a href="#{{x.id_project}}" aria-controls="{{x.id_project}}" role="tab" data-toggle="tab">{{x.category}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="q_p container-fluid container-clean" id="categories" ng-controller="categories" ng-show="intro.categories.value">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="h-section-header">Exploră categoriile</h2>
                        </div>
                    </div>
                    <div class="row categories">
                        <div class="col-xs-6 col-sm-4 col-md-3" ng-repeat="q in data">
                            <div class="content">
                                <a href="#" class="cat-img" style="background-image: url('{{q.img}}')">
                                    <div class="content-overlay">
                                        <div class="content-overlay-text">
                                            {{q.name}}
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="q_p container-fluid container-color" id="popularProjects" ng-controller="popularProjects" ng-show="intro.popularProjects.value">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="h-section-header">Cele mai populare proiecte</h2>
                        </div>
                    </div>
                    <div class="row projects">
                        <div class="col-sm-4" ng-repeat="q in data">
                            <a href="#">
                                <div class="content q_todayProject col-img col-eq" style="background-image: url('{{q.img}}')">
                                </div>
                            </a>
                            <div class="content q_todayProject col-info col-eq">
                                <div class="projectDetails put-top">
                                    <h4>{{q.name}}</h4>
                                    <a href="#" class="sub">by {{q.user}}</a>
                                    <p>{{q.description}}</p>
                                </div>
                                <div class="projectDetails put-bottom">
                                    <div class="info">
                                        <ul class="list">
                                            <li><a href="#"><i class="fa fa-tag fa-rotate-90"></i> {{q.category}}</a></li>
                                            <li><a href="#"><i class="fa fa-map-marker"></i> {{q.location}}</a></li>
                                        </ul>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{q.funded}}" aria-valuemin="0" aria-valuemax="100" style="width: {{q.funded}}%;">
                                        </div>
                                    </div>
                                    <ul class="list">
                                        <li><span class="bold">{{q.fundedPercent}}%</span><span class="text">funded</span></li>
                                        <li><span class="bold">${{q.pledged}}</span><span class="text">pledged</span></li>
                                        <li><span class="bold">{{q.daysLeft}}</span><span class="text">days to go</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="q_p q_pb-0 container-fluid container-clean" id="getStarted">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="h-section-header big text-center q_mb-small q_mt-0">Începe azi!</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-primary btn-lg btn-block dirty">{{intro.button_text_1.value}}</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="btn btn-primary btn-lg btn-block dirty">{{intro.button_text_2.value}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include $put['footer']; include $put['foot']; ?>
    </body>

    </html>
