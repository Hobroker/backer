<?php
session_start();
$in = isset($_SESSION['id_user']);
$admin = isset($_SESSION['admin']) && $_SESSION['admin'] == '1';

$put = array(
    'head' => 'include/head.php',
    'navbar' => 'include/navbar.php',
    'foot' => 'include/foot.php',
    'footer' => 'include/footer.php'
);
$text_class = array(
    'c_accent' => 'container-up bg-color-accent',
    'c_primary' => 'container-up bg-color-primary',
    'c_text' => 'container-up bg-color-text',
    'text' => 'container-is-up'
);
$allowed_img = array(
    'png',
    'jpg',
    'gif',
    'jpeg'
);
if (isset($sub)) {
    foreach ($put as $key => $value) {
        $put[$key] = "../$value";
    }
}

function protect_god() {
    $admin = isset($_SESSION['admin']) && $_SESSION['admin'] == '1';
    if (!$admin) {
        $bitch_please = array(
            'profile',
            'dashboard-user',
            'page-projects',
            'page-files',
            'page-pledges'
        );
        if (!in_array($_GET['page'], $bitch_please)) {
            goHome();
        }
    }
}

function goHome() {
    header('Location: ../');
    die('WTF?');
    // echo $_SESSION['admin'];
    
    
}
function file_newname($path, $filename) {
    if ($pos = strrpos($filename, '.')) {
        $name = substr($filename, 0, $pos);
        $ext = substr($filename, $pos);
    } 
    else {
        $name = $filename;
    }
    $newpath = $path . '/' . $filename;
    $newname = $filename;
    $counter = 1;
    while (file_exists($newpath)) {
        $newname = $name . ' (' . $counter . ')' . $ext;
        $newpath = $path . '/' . $newname;
        $counter++;
    }
    return $newname;
}
?>
